package routers

import (
	"gitlab.com/intatch/new-sport.git/sport-new/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
}
